package edu.bo.examen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var userViewModel: UserViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userViewModel = UserViewModel(UserRepository())
        userViewModel.model.observe(this, Observer(::updateUi))
        buttonSave.setOnClickListener {
            userViewModel.doSaveUser(name.text.toString(),lastname.text.toString(),email.text.toString())
        }
    }
    private fun updateUi(model: UserViewModel.UIModel?) {
        if ( model is UserViewModel.UIModel.Loading) {
            progressBar.visibility = View.VISIBLE
            group.visibility = View.GONE
        } else{
            progressBar.visibility = View.GONE
            group.visibility = View.VISIBLE
        }
        when ( model ) {
            is UserViewModel.UIModel.SaveUser -> validateSaveUser(model.success)
        }
    }

    private fun validateSaveUser( response: Boolean) {
        if(response){
            val builder = AlertDialog.Builder( applicationContext )
            builder.setTitle( "Registro completado" )
            builder.setMessage( "Usuario guardado satisfactoriamente" )
            builder.show()
        }else{
            val builder = AlertDialog.Builder( applicationContext)
            builder.setTitle( "Error de datos" )
            builder.setMessage( "Verifique los datos e intente nuevamente por favor" )
            builder.show()
        }

    }
}
