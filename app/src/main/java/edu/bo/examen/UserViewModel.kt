package edu.bo.examen

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class UserViewModel(val userRepository: UserRepository) : ViewModel()  {
    val model: LiveData<UIModel>
        get() = _model
    private val _model = MutableLiveData<UIModel>()

    sealed class UIModel {
        class SaveUser(val success: Boolean): UIModel()
        object Loading: UIModel()
    }

    fun doSaveUser(name: String, LastName: String, email: String) {
        val newUser = User(name,LastName,email)
        _model.value = UIModel.Loading
        val runnable = Runnable {
            _model.value = UIModel.SaveUser(userRepository.saveUserInRepository(newUser))
        }
        Handler().postDelayed(runnable, 3000)
    }
}